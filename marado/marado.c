

#include <stdio.h>    // para usar printf
#include <stdlib.h>     // para usar exit y funciones de la libreria standard
#include <pthread.h>    // para usar threads
#include <unistd.h>    // para hacer sleep
#include <semaphore.h>  
#include <string.h>   


sem_t MA;
sem_t RA;
sem_t DO;

pthread_mutex_t mutex;

void *funcionMA (void *args){
	//int *numero=arg;
	//int nf= *numero;
	char *cadena=args;
	for (int i = 0; i < strlen(cadena)-1; ++i)
	{
	sem_wait(&MA);
	pthread_mutex_lock(&mutex);
	printf("MA ");
	sem_post(&RA);
	pthread_mutex_unlock(&mutex);
	}
	//pthread_exit(NULL);
	
}
void *funcionRA(void *args){
	char *cadena=args;
	for (int i = 0; i < strlen(cadena)-1; ++i)
	{
	sem_wait(&RA);
	pthread_mutex_lock(&mutex);
	printf("RA ");
	sem_post(&DO);
	pthread_mutex_unlock(&mutex);
	}
	//pthread_exit(NULL);
	
}
void* funcionDO(void *args){
	char *cadena=args;
	for (int i = 0; i < strlen(cadena)-1; ++i)
	{
		sem_wait(&DO);
	pthread_mutex_lock(&mutex);
	printf("DOOO\n");
	sem_post(&MA);
	pthread_mutex_unlock(&mutex);	
	}
	//pthread_exit(NULL);
	
}


int main()
{
	int numero;
	printf("Cantidad de cantos\n");
	scanf("%i",&numero);
	char *cadena;	

	for (int i = 0; i < numero; ++i)
	{
		strcat(cadena,"1");
	}
	

	sem_init(&MA,0,1);
	sem_init(&RA,0,0);
	sem_init(&DO,0,0);

	pthread_mutex_init ( &mutex, NULL);  //Inicializa el mutex

	
	//int *numero=(int *)5;
	pthread_t h1;
    pthread_t h2;
	pthread_t h3;

	pthread_create(&h1,NULL,funcionMA,(void *)cadena);
	pthread_create(&h2,NULL,funcionRA,(void *)cadena);
	pthread_create(&h3,NULL,funcionDO,(void *)cadena);

	pthread_join(h1,NULL);
	pthread_join(h2,NULL);
	pthread_join(h3,NULL);


	pthread_exit(NULL);

	sem_destroy(&MA);
	sem_destroy(&RA);
	sem_destroy(&DO);

	pthread_mutex_destroy(&mutex);
	
}
