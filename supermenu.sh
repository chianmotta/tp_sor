#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
actual=$(pwd)
proyectoActual=$actual;
proyectos="/home/gian/Escritorio/TP";
git remote add origin https://gitlab.com/chianmotta/tp_sor.git;
git config --global user.email "mottagian9703@gmail.com";
git config --global user.name "Gian Motta";

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Buscar Programa";
    echo -e "\t\t\t c.  Filtrar por extension";
    echo -e "\t\t\t d.  Buscar Linea";        
    echo -e "\t\t\t e.  Estados del proceso";
    echo -e "\t\t\t f.  Subir a GIT";
    echo -e "\t\t\t g.  Bajar de GIT";
    echo -e "\t\t\t h.  Crear ejecutable";          
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
    echo $1;
    while true; do
        echo "desea ejecutar? (s/n)";
            read respuesta;
            case $respuesta in
                [Nn]* ) break;;
                   [Ss]* ) eval $1
                break;;
                * ) echo "Por favor tipear S/s ó N/n.";;
            esac
    done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
        imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
        decidir "cd $proyectoActual; git status";
}

b_funcion () {
           imprimir_encabezado "\tOpción B. Buscar Programa";
       read programa
       if dpkg -l|grep -w $programa; then
        echo "El programa se encuentra instalado"
       else
                echo "no encontrado"
       fi
}

c_funcion () {
          imprimir_encabezado "\tOpción Filtrar archivos por extension";
          echo "Ingrese path"
          read path
          cd $path
          echo "Ingrese Extension"
          read extension
          echo "Ingrese Nombre"
          read nombre
      find *.$extension | grep -i $nombre
          
            
}

d_funcion () {
    imprimir_encabezado "\tOpción Buscar Linea";
        echo "Ingrese Cadena "
        read cadena
        echo "Ingrese Path"
        read path
        cat $path | grep -i $cadena >> salida.out


}


e_funcion () {
    imprimir_encabezado "\tOpción e";

    ./gEstados.sh & 
    top | grep $!

    #ps aux | grep alanrebuenamigo
}

f_funcion(){
    imprimir_encabezado "\tOpción f Guardar Cambios";
    cd $proyectoActual;
    decidir "git add -A";
    echo "Añada un comentario"
    read comment;
    decidir 'git commit -m "'"$comment"'"';
    decidir "git push -u origin master "; #Para que ande el origin master, hay que poner "git remote add origin https://gitlab.com/chianmotta/tp_sor.git"
}


g_funcion(){
    imprimir_encabezado "\tOpción g Obtener ultima version";
    cd $proyectoActual;
    decidir "git pull";    
}

h_funcion(){
    imprimir_encabezado "\tOpción h Crear Ejecutable";
    #echo Ingrese usuario
    #read usuario
     var=$(pwd)
     #echo $var  
        echo [Desktop Entry]>>supermenu.desktop
        echo Name=SuperMenu>>supermenu.desktop
        echo Comment=Consigna TP>>supermenu.desktop
        echo Exec= $var'/supermenu.sh' >>supermenu.desktop
        echo Icon= $var'/icon.png'>>supermenu.desktop
        echo Terminal=true>>supermenu.desktop
        echo Type=Application>>supermenu.desktop

    chmod 777 $var'/supermenu.desktop'
    echo 'Su ejecutable se creo en:'$var


}
#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
        h|H) h_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
 

