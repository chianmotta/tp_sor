#include <stdio.h>    // para usar printf
#include <stdlib.h>     // para usar exit y funciones de la libreria standard
#include <pthread.h>    // para usar threads
#include <unistd.h>    // para hacer sleep
#include <semaphore.h>  
#include <string.h> 

sem_t ole;
sem_t ola;
sem_t cadaDia;
sem_t ohArg;
sem_t sentimiento;
sem_t aux;



void* fOle(void *args){
	char *cadena=args;

	for (int i = 0; i < ((strlen(cadena))*2)-2; ++i){
		sem_wait(&ole);
		printf("ole ole ole \n");
		sem_post(&aux);		
	}
	//pthread_exit(NULL);
}	

void* fOla(void *args){
	char *cadena=args;
	for (int i = 0; i < strlen(cadena)-1; ++i){
	sem_wait(&aux);
	sem_wait(&ola);	
	printf("ole ole ole ola \n");
	sem_post(&ole);
	sem_post(&cadaDia);
	}
	//pthread_exit(NULL);
	
	
}

void* fCadaDia(void *args){
	char *cadena=args;
	for (int i = 0; i < strlen(cadena)-1; ++i){
	//sem_wait(&aux);
	sem_wait(&cadaDia);
	printf("cada dia te quiero mas \n");
	sem_post(&ohArg);
	}
	//pthread_exit(NULL);
	
}

void* fOhArg(void *args){

	char *cadena=args;
	for (int i = 0; i < strlen(cadena)-1; ++i){
	sem_wait(&ohArg);
	printf("oooh Argentina \n");
	sem_post(&sentimiento);
	}
	//pthread_exit(NULL);

}

void* fSentimiento(void *args){
	char *cadena=args;
	for (int i = 0; i < strlen(cadena)-1; ++i){
	sem_wait(&sentimiento);		
	printf("es un sentimiento, no puedo parar \n\n");
	sem_post(&ole);
	sem_post(&ola);
	}
}

int main(){
int cantidadCantos = 0;
printf("Cantidad de cantos\n");
scanf("%i",&cantidadCantos);

char *cadena;	

	for (int i = 0; i < cantidadCantos; ++i)
	{
		strcat(cadena,"1");
	}



	sem_init(&ole,0,1);
	sem_init(&ola,0,1);
	sem_init(&cadaDia,0,0);
	sem_init(&ohArg,0,0);
	sem_init(&sentimiento,0,0);
	sem_init(&aux,0,0);

	pthread_t h1;
	pthread_t h2;
	pthread_t h3;
	pthread_t h4;
	pthread_t h5;

	pthread_create(&h1, NULL, fOle, (void *)cadena);
	pthread_create(&h2, NULL, fOla, (void *)cadena);
	pthread_create(&h3, NULL, fCadaDia, (void *)cadena);
	pthread_create(&h4, NULL, fOhArg, (void *)cadena);
	pthread_create(&h5, NULL, fSentimiento, (void *)cadena);

	pthread_join(h1, NULL);
	pthread_join(h2, NULL);
	pthread_join(h3, NULL);
	pthread_join(h4, NULL);
	pthread_join(h5, NULL);	
	



	pthread_exit(NULL);

	sem_destroy(&ole);
	sem_destroy(&ola);
	sem_destroy(&cadaDia);
	sem_destroy(&ohArg);
	sem_destroy(&sentimiento);
	sem_destroy(&aux);


}
